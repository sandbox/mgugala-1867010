<?php 
/**
 * @file 
 * Administration pages for audio & video settings.
 */

// ----------------------------------------------------------------------------
// Forms 

/**
 * Global settings of Audio & Video module.
 */
function avmedia_settings_form() {
  drupal_add_js(drupal_get_path('module', 'avmedia') . '/avmedia.js');

  $settings = avmedia_get_settings();
  $libraries = libraries_get_libraries();
  $avlibs = avmedia_library();

  // Fieldsets
  $form['library'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site library'),
    '#weight' => 0,
    '#collapsible' => FALSE,
  );

  // Fields
  $i = 2;
  $options = array();
  foreach ($avlibs as $lid => $library) {
    if (isset($libraries[$lid])) {
      // Add library
      drupal_add_library('avmedia', $lid);

      $options[$lid] = $library['title'];

      $form[$lid] = array(
        '#type' => 'fieldset',
        '#title' => t('@name library', array('@name' => $library['title'] )),
        '#weight' => $i++,
        '#collapsible' => FALSE,
      );

      // Generate <video> tag with example video
      $video  = '<video class="' . $lid . '">';
      foreach ( $library['video']['codecs'] as $codec_type => $file_url ) {
        $video .= '<source src="' . $file_url . '" type="video/' . $codec_type . '" title="' . strtoupper($codec_type) . '" />';
      }
      foreach ( $library['video']['subtitles'] as $lang => $file_url ) {
        $video .= '<track kind="subtitles" src="' . $file_url . '" srclang="' . strtoupper($lang) . '" type="text/srt" />';
      }
      $video .= '</video>';

      $form[$lid]['codec'] = array(
        '#markup' => $video,
      );
    }
  }

  $form['library']['avmedia_library_default'] = array(
    '#type' => 'select',
    '#title' => t('Site library'),
    '#options' => $options,
    '#default_value' => $settings->library_default,
  );

  $form['actions'] = array('#type' => 'actions', '#weight' => 1);
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  return $form;
}
/**
 * Codecs settings of Audio & Video module.
 */
function avmedia_codecs_form() {
  $form['weight'] = array('#tree' => TRUE);

  foreach (avmedia_get_codecs() as $codec) {
    $options[$codec->id] = '';
    if ( $codec->enabled ) {
      $enabled[] = $codec->id;
    }

    $form['weight'][$codec->id] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $codec->label)),
      '#title_display' => 'invisible',
      '#default_value' => $codec->weight,
      '#attributes' => array('class' => array('codecs-order-weight')),
    );
    $form['name'][$codec->id] = array( '#markup' => check_plain( $codec->label ) );
  }

  $form['enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled codecs'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => empty($enabled) ? array() : $enabled,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  $form['#theme'] = 'avmedia_codecs_form';

  return $form;
}
/**
 * Allows to add new codec.
 */
function avmedia_codec_add_form( $form ) {
  $form['codec_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Codec name'),
    '#maxlength' => 255,
    '#size' => 45,
    '#description' => t(''),
    '#required' => TRUE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Add codec'));

  return $form;
}
/**
 * Allows to edit codec.
 */
function avmedia_codec_edit_form( $form, &$form_state, $langcode ) {
  $codecs = avmedia_get_codecs();

  if (!isset($codecs[$form_state['build_info']['args'][0]])) {
    drupal_set_message(t('There is no codec with id %id', array('%id' => $form_state['build_info']['args'][0])), 'error');
    return $form;
  }

  $form['codec_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Codec name'),
      '#maxlength' => 255,
      '#size' => 45,
      '#description' => t(''),
      '#default_value' => $codecs[$form_state['build_info']['args'][0]]->label,
      '#required' => TRUE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}
/**
 * Allows to delete codec.
 */
function avmedia_codec_delete_form( $form, &$form_state, $langcode ) {
  // Do not allow deletion of default codec
  if ($form_state['build_info']['args'][0] == 1) {
    drupal_set_message(t('The default codec cannot be deleted.'), 'error');
    drupal_goto('admin/config/media/avmedia/codecs');
  }

  $codecs = avmedia_get_codecs();

  // For other languages, warn user that data loss is ahead.
  $languages = language_list();

  if ($form_state['build_info']['args'][0] === FALSE) {
    drupal_not_found();
    drupal_exit();
  }
  else {
    $form['codec_id'] = array('#type' => 'value', '#value' => $codecs[$form_state['build_info']['args'][0]]->id);
    return confirm_form($form, t('Are you sure you want to delete the codec %name?', array('%name' => t($codecs[$form_state['build_info']['args'][0]]->label))), 'admin/config/media/avmedia/codecs', t('Deleting a language will. This action cannot be undone.'), t('Delete'), t('Cancel'));
  }
}
/**
 * Subtitles settings of Audio & Video module.
 */
function avmedia_subtitles_form() {
  $form['weight'] = array('#tree' => TRUE);

  foreach (avmedia_get_languages() as $code => $lang) {
    $options[$lang->lang] = '';
    if ( $lang->enabled ) {
      $enabled[] = $lang->lang;
    }
    
    $form['weight'][$code] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $lang->name)),
      '#title_display' => 'invisible',
      '#default_value' => $lang->weight,
      '#attributes' => array('class' => array('subtitles-order-weight')),
    );
    $form['name'][$code] = array( '#markup' => check_plain( $lang->name ) );
  }
  
  $form['enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled languages'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => empty($enabled) ? array() : $enabled,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  $form['#theme'] = 'avmedia_subtitles_form';

  return $form;
}
/**
 * Allows to add new codec.
 */
function avmedia_language_add_form( $form ) {
  $predefined = _locale_prepare_predefined_list();
  $form['langcode'] = array('#type' => 'select',
      '#title' => t('Subtitles language'),
      '#default_value' => key($predefined),
      '#options' => $predefined,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Add subtitles language'));

  return $form;
}
/**
 * Allows to delete codec.
 */
function avmedia_language_delete_form( $form, &$form_state, $langcode ) {
  $languages = avmedia_get_languages();
  $lang = $form_state['build_info']['args'][0];

  if ($lang === FALSE) {
    drupal_not_found();
    drupal_exit();
  }
  else {
    $form['codec_id'] = array('#type' => 'value', '#value' => $languages[$lang]->name);
    return confirm_form($form, t('Are you sure you want to delete the language %name?', array('%name' => t($languages[$lang]->name))), 'admin/config/media/avmedia/subtitles', t('Deleting a language will. This action cannot be undone.'), t('Delete'), t('Cancel'));
  }
}

// ----------------------------------------------------------------------------
// Submit handlers

/**
 * Process general settings form submissions.
 */
function avmedia_settings_form_submit($form, &$form_state) {
  $settings = avmedia_get_settings();
  $settings->library_default = $form_state['values']['avmedia_library_default'];
  avmedia_set_settings($settings);
  drupal_set_message(t('Configuration saved.'));
  $form_state['redirect'] = 'admin/config/media/avmedia';
  return;
}
/**
 * Process codecs general settings form submissions.
 */
function avmedia_codecs_form_submit($form, &$form_state) {
  $codecs = avmedia_get_codecs();
  $defaults = avmedia_get_settings();

  foreach ($codecs as $codec) {
    if ($codec->id == 1) {
      // Automatically enable the default codec and the codec
      // which was default previously (because we will not get the
      // value from that disabled checkbox).
      $form_state['values']['enabled'][$codec->id] = 1;
    }

    if ($form_state['values']['enabled'][$codec->id]) {
      $codec->enabled = 1;
    } else {
      $codec->enabled = 0;
    }

    $codec->weight = $form_state['values']['weight'][$codec->id];
    $codecs[$codec->id] = $codec;
  }

  avmedia_set_codecs(avmedia_sort_codecs($codecs));

  drupal_set_message(t('Configuration saved.'));
  $form_state['redirect'] = 'admin/config/media/avmedia/codecs';
}
/**
 * Process the codec addition form submission.
 */
function avmedia_codec_add_form_submit( $form, &$form_state ) {
  $codecs = avmedia_get_codecs();
  $defaults = avmedia_get_settings();

  $codecs[$defaults->maxid + 1] = (object) array('id' => $defaults->maxid + 1, 'label'   => check_plain($form_state['values']['codec_name']), 'enabled' => 1, 'weight' => 10);
  avmedia_set_codecs($codecs);

  $defaults->maxid++;
  avmedia_set_settings($defaults);

  drupal_set_message(t('The codec %codec has been created and can now be used.', array('%codec' => t($form_state['values']['codec_name']))));
  $form_state['redirect'] = 'admin/config/media/avmedia/codecs';
}
/**
 * Process the codec edit form submission.
 */
function avmedia_codec_edit_form_submit( $form, &$form_state ) {
  $codecs = avmedia_get_codecs();

  $codecs[$form_state['build_info']['args'][0]]->label = check_plain($form_state['values']['codec_name']);
  avmedia_set_codecs($codecs);

  drupal_set_message(t('The codec %codec updated.', array('%codec' => t($form_state['values']['codec_name']))));

  $form_state['redirect'] = 'admin/config/media/avmedia/codecs';
}
/**
 * Process the codec deletion form submission.
 */
function avmedia_codec_delete_form_submit( $form, &$form_state ) {
  $codecs = avmedia_get_codecs();
  $label  = $codecs[$form_state['build_info']['args'][0]]->label;
  unset($codecs[$form_state['build_info']['args'][0]]);
  avmedia_set_codecs($codecs);

  drupal_set_message(t('The codec %codec deleted.', array('%codec' => t($label))));
  $form_state['redirect'] = 'admin/config/media/avmedia/codecs';
}
/**
 * Process codecs general settings form submissions.
 */
function avmedia_subtitles_form_submit($form, &$form_state) {
  $languages = avmedia_get_languages();

  foreach ($languages as $lang => $info) {
    $info->weight = $form_state['values']['weight'][$lang];
    $languages[$lang] = $info;
    
    if ($form_state['values']['enabled'][$lang]) {
      $languages[$lang]->enabled = 1;
    } else {
      $languages[$lang]->enabled = 0;
    }
  }

  avmedia_set_languages(avmedia_sort_languages($languages));

  drupal_set_message(t('Configuration saved.'));

  cache_clear_all('*', 'cache_page', TRUE);

  $form_state['redirect'] = 'admin/config/media/avmedia/subtitles';
  return;
}
/**
 * Process the codec addition form submission.
 */
function avmedia_language_add_form_submit( $form, &$form_state ) {
  $languages = avmedia_get_languages();
  $predefined = _locale_prepare_predefined_list();

  $languages[check_plain($form_state['values']['langcode'])] = (object) array( 'lang' => check_plain($form_state['values']['langcode']), 'name' => check_plain($predefined[$form_state['values']['langcode']]), 'weight' => 10);
  avmedia_set_languages($languages);

  drupal_set_message(t('The subtitles language %lang has been created and can now be used.', array('%lang' => t($form_state['values']['langcode']))));

  $form_state['redirect'] = 'admin/config/media/avmedia/subtitles';
}
/**
 * Process the codec deletion form submission.
 */
function avmedia_language_delete_form_submit( $form, &$form_state ) {
  $languages = avmedia_get_languages();
  $label = $languages[$form_state['build_info']['args'][0]]->name;

  unset($languages[$form_state['build_info']['args'][0]]);
  avmedia_set_languages($languages);

  drupal_set_message(t('The subtitles language %language deleted.', array('%language' => t($label))));

  $form_state['redirect'] = 'admin/config/media/avmedia/subtitles';
}

// ----------------------------------------------------------------------------
// Module functionality

/**
 * Set audio and video codecs global settings
 */
function avmedia_sort_codecs( $codecs ) {
  usort($codecs, function($a, $b) {
    return $a->weight > $b->weight;
  });
  $keyed_codecs = array();
  foreach($codecs as $codec) {
    $keyed_codecs[$codec->id] = $codec;
  }
  return $keyed_codecs;
}
/**
 * Set video subtitles global settings
 */
function avmedia_sort_languages( $subtitles ) {
  usort($subtitles, function($a, $b) {
    return $a->weight > $b->weight;
  });
  $languages = array();
  foreach($subtitles as $lang) {
    $languages[$lang->lang] = $lang;
  }
  return $languages;
}

// ----------------------------------------------------------------------------
// Form theme functions (alphabetical order)

/**
 * Returns HTML for a group of file upload widgets.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the widgets.
 *
 * @ingroup themeable
 */
function theme_avmedia_settings_form( $variables ) {
  /*$form = $variables['form'];
   $default = language_default();
  foreach ($form['name'] as $key => $element) {
  // Do not take form control structures.
  if (is_array($element) && element_child($key)) {
  // Disable checkbox for the default language, because it cannot be disabled.
  if ($key == $default->language) {
  $form['enabled'][$key]['#attributes']['disabled'] = 'disabled';
  }

  // Add invisible labels for the checkboxes and radio buttons in the table
  // for accessibility. These changes are only required and valid when the
  // form is themed as a table, so it would be wrong to perform them in the
  // form constructor.
  $title = drupal_render($form['name'][$key]);
  $form['enabled'][$key]['#title'] = t('Enable !title', array('!title' => $title));
  $form['enabled'][$key]['#title_display'] = 'invisible';
  $form['codec_default'][$key]['#title'] = t('Set !title as default', array('!title' => $title));
  $form['codec_default'][$key]['#title_display'] = 'invisible';
  $rows[] = array(
      'data' => array(
          '<strong>' . $title . '</strong>',
          drupal_render($form['native'][$key]),
          check_plain($key),
          drupal_render($form['direction'][$key]),
          array('data' => drupal_render($form['enabled'][$key]), 'align' => 'center'),
          drupal_render($form['codec_default'][$key]),
          drupal_render($form['weight'][$key]),
          l(t('edit'), 'admin/config/regional/language/edit/' . $key) . (($key != 'en' && $key != $default->language) ? ' ' . l(t('delete'), 'admin/config/regional/language/delete/' . $key) : '')
      ),
      'class' => array('draggable'),
  );
  }
  }
  $header = array(array('data' => t('English name')), array('data' => t('Native name')), array('data' => t('Code')), array('data' => t('Direction')), array('data' => t('Enabled')), array('data' => t('Default')), array('data' => t('Weight')), array('data' => t('Operations')));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'language-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('language-order', 'order', 'sibling', 'language-order-weight');*/
  $output = '';

return $output;
}
/**
 * Returns HTML for a group of file upload widgets.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the widgets.
 *
 * @ingroup themeable
 */
function theme_avmedia_codecs_form( $variables ) {
  $form = $variables['form'];
  $defaults = avmedia_get_settings();

  foreach ( $form['name'] as $key => $element ) {
    // Do not take form control structures.
    if (is_array($element) && element_child($key) ) {
      // Disable checkbox for the default language, because it cannot be disabled.
      if ( $key == 1 ) {
        $form['enabled'][$key]['#attributes']['disabled'] = 'disabled';
      }

      // Add invisible labels for the checkboxes and radio buttons in the table
      // for accessibility. These changes are only required and valid when the
      // form is themed as a table, so it would be wrong to perform them in the
      // form constructor.
      $title = drupal_render( $form['name'][$key] );
      $form['enabled'][$key]['#title'] = t('Enable !title', array('!title' => $title) );
      $form['enabled'][$key]['#title_display'] = 'invisible';
      $rows[] = array(
        'data' => array(
          '<strong>' . $title . '</strong>' . (($key == 1) ? ' (default)' : ''),
          array('data' => drupal_render($form['enabled'][$key]), 'align' => 'center'),
          drupal_render($form['weight'][$key]),
          l( t('edit'), 'admin/config/media/avmedia/codecs/edit/' . $key ) . ( ( $key != 1 ) ? ' ' . l( t('delete'), 'admin/config/media/avmedia/codecs/delete/' . $key) : '')
        ),
        'class' => array('draggable'),
      );
    }
  }
  $header = array(t('Codec name'), t('Enabled'), t('Weight'), t('Operations'));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'codecs-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('codecs-order', 'order', 'sibling', 'codecs-order-weight', NULL, NULL, TRUE, 0);

  return $output;
}
/**
 * Returns HTML for a group of file upload widgets.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the widgets.
 *
 * @ingroup themeable
 */
function theme_avmedia_subtitles_form( $variables ) {
  $form = $variables['form'];
  $defaults = avmedia_get_settings();

  foreach ( $form['name'] as $key => $element ) {
    // Do not take form control structures.
    if (is_array($element) && element_child($key) ) {
      // Add invisible labels for the checkboxes and radio buttons in the table
      // for accessibility. These changes are only required and valid when the
      // form is themed as a table, so it would be wrong to perform them in the
      // form constructor.
      $title = drupal_render( $form['name'][$key] );
      $form['enabled'][$key]['#title'] = t('Enable !title', array('!title' => $title) );
      $form['enabled'][$key]['#title_display'] = 'invisible';
      $rows[] = array(
          'data' => array(
              '<strong>' . $title . '</strong>',
              array('data' => drupal_render($form['enabled'][$key]), 'align' => 'center'),
              drupal_render($form['weight'][$key]),
              l( t('delete'), 'admin/config/media/avmedia/subtitles/delete/' . $key)
          ),
          'class' => array('draggable'),
      );
    }
  }
  $header = array(t('Language'), t('Enabled'), t('Weight'), t('Operations'));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'subtitles-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('subtitles-order', 'order', 'sibling', 'subtitles-order-weight', NULL, NULL, TRUE, 0);

  return $output;
}