(function($, Drupal, undefined){
  /**
   * When set to enable mediaelement for all audio/video files add it to the page.
   */
  Drupal.behaviors.mediaelement = {
    attach: function(context, settings) {

      $('video.mediaelement,audio.mediaelement', context).once('mediaelement', function() {
        $(this).mediaelementplayer({
	      features: ['playpause','progress','current','duration','tracks','volume','fullscreen','sourcechooser']
        });
      });
    }
  };
})(jQuery, Drupal);