<?php 
/**
 * @file 
 * Defines supported audio & video libraries.
 */

// Load file with definied libraries
require_once drupal_get_path( 'module', 'libraries' ) . '/libraries.module';

/**
 * Define libraries supported by Audio & Video modules
 * 
 * @return array with information of supported libraries
 */
function _avmedia_get_libraries() {
	return array(
		'mediaelement' => array(
			'title' => 'MediaElement',
			'website' => 'http://mediaelementjs.com/',
			'version' => '2.1.6',
			'js' => array(
				libraries_get_path('mediaelement') . '/build/mediaelement-and-player.min.js' => array('group' => JS_LIBRARY, 'preprocess' => FALSE),
				libraries_get_path('mediaelement') . '/src/js/mep-feature-sourcechooser.js' => array('group' => JS_LIBRARY, 'preprocess' => FALSE),
			),
			'css' => array(
				libraries_get_path('mediaelement') . '/build/mediaelementplayer.min.css' => array('group' => CSS_SYSTEM),
			),
			'video' => array(
				'codecs' => array(
					'mp4'  => '/' . libraries_get_path('mediaelement') . '/media/echo-hereweare.mp4',
					'ogv'  => '/' . libraries_get_path('mediaelement') . '/media/echo-hereweare.ogv',
					'webm' => '/' . libraries_get_path('mediaelement') . '/media/echo-hereweare.webm',
				),
				'subtitles' => array(
					'en'   => '/' . libraries_get_path('mediaelement') . '/media/mediaelement.srt',
				),
			),
			'audio' => array(
				'codecs' => array(
					'mp3'  => '/' . libraries_get_path('mediaelement') . '/media/AirReview-Landmarks-02-ChasingCorporate.mp3',
				),
			),
		),
	);
}