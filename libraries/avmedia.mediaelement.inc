<?php 
/**
 * @file 
 * 
 */

/**
 * Returns HTML for help text based on file upload validators.
 *
 * @param $variables
 *   An associative array containing:
 *   - description: The normal description for this field, specified by the
 *     user.
 *   - upload_validators: An array of upload validators as used in
 *     $element['#upload_validators'].
 *
 * @ingroup themeable
 */
function theme_avmedia_video_mediaelement($variables) {
	$output = '<div class="mediaelement-video">';
	$output .= '<video class="mediaelement" style="width:100%;height:100%;">';
	
	foreach ($variables['files']['codecs'] as $file) {
	  $ex_start = strrpos($file['src'], '.') + 1;
	  $ex_length = strlen($file['src']) - strrpos($file['src'], '.') - 1;
	  $ext = substr($file['src'], $ex_start, $ex_length);
	  
	  $output .= '<source src="' . $file['src'] . '" type="video/' . $ext . '" title="' . $file['label'] . '" />';
	}
	
  if (isset($variables['files']['subtitles'])) {
  	foreach ($variables['files']['subtitles'] as $file) {
  		$ex_start = strrpos($file['src'], '.') + 1;
  		$ex_length = strlen($file['src']) - strrpos($file['src'], '.') - 1;
  		$ext = substr($file['src'], $ex_start, $ex_length);
  		$output .= '<track kind="subtitles" src="' . $file['src'] . '" type="text/' . $ext . '" srclang="' . $file['label'] . '" />';
  	}
  }
	
	$output .= '</video>';
	$output .= '</div>';

	return $output;
}
/**
 * Returns HTML for help text based on file upload validators.
 *
 * @param $variables
 *   An associative array containing:
 *   - description: The normal description for this field, specified by the
 *     user.
 *   - upload_validators: An array of upload validators as used in
 *     $element['#upload_validators'].
 *
 * @ingroup themeable
 */
function theme_avmedia_audio_mediaelement($variables) {
  $output = '<div class="mediaelement-audio">';
	$output .= '<audio id="test" class="mediaelement" style="width:100%;">';
	
  foreach ($variables['files']['codecs'] as $file) {
	  $ex_start = strrpos($file['src'], '.') + 1;
	  $ex_length = strlen($file['src']) - strrpos($file['src'], '.') - 1;
	  $ext = substr($file['src'], $ex_start, $ex_length);
	  
	  $output .= '<source src="' . $file['src'] . '" type="audio/' . $ext . '" title="' . $file['label'] . '" />';
	}
	
	$output .= '</audio>';
	$output .= '</div>';

	return $output;
}