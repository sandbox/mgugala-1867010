<?php 
/**
 * @file
 * Field module functionality for Audio & Video module.
 * Defines two field types:
 *  - audio
 *  - video
 */

// Load file with audio and video fields definition
require_once drupal_get_path( 'module', 'avmedia' ) . '/avmedia.collection.inc';

// Load file with definied libraries
require_once drupal_get_path( 'module', 'avmedia' ) . '/libraries/avmedia.libraries.inc';

// Load definied libraries formatters
foreach (avmedia_library() as $lid => $library) {
	require_once drupal_get_path( 'module', 'avmedia' ) . '/libraries/avmedia.' . $lid . '.inc';
}

/**
 * Implements hook_field_info().
 *
 * Basic description of the field.
 */
function avmedia_field_info() {
  $settings = avmedia_get_settings();
  return array(
    'avmedia_video' => array(
      'label' 			=> t('Video'),
      'description' => t('This field stores information about video.'),
      'settings' => array(
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
      ),
      'instance_settings' => array(
        'file_extensions' => 'mp4 ogv webm',
        'file_subtitles_extensions' => 'srt txt',
        'file_directory' => 'video',
        'file_subtitles_directory' => 'subtitles',
        'max_filesize' => '',
        'max_filesize_subtitles' => '',
        'progress_indicator' => 'throbber',
      ),
      'default_widget'    => 'avmedia_video',
      'default_formatter' => 'avmedia_video_' . $settings->library_default,
    ),
    'avmedia_audio' => array(
      'label' 			=> t('Audio'),
      'description' => t('This field stores information about audio.'),
      'settings' => array(
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
      ),
      'instance_settings' => array(
        'file_extensions'    => 'mp3',
        'file_directory' 	   => 'audio',
        'max_filesize' 		   => '',
        'progress_indicator' => 'throbber',
      ),
      'default_widget'    => 'avmedia_audio',
      'default_formatter' => 'avmedia_audio_' . $settings->library_default,
    ),
  );
}
/**
 * Implements hook_field_settings_form().
 */
function avmedia_field_settings_form($field, $instance, $has_data) {
  $settings = array_merge(field_info_field_settings($field['type']), $field['settings']);
  // Schema options
  $scheme_options = array();
  foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
    $scheme_options[$scheme] = $stream_wrapper['name'];
  }
  $form['uri_scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Upload destination'),
    '#options' => $scheme_options,
    '#default_value' => $settings['uri_scheme'],
    '#description' => t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
    '#disabled' => $has_data,
  );
  return $form;
}
/**
 * Implements hook_field_instance_settings_form().
 *
 * Defines instance specific settings like:
 *  - upload directory
 *  - allowed file extensions
 *  - max filesize
 *  - codecs support settings
 *  - subtitles settings
 */
function avmedia_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  // Add audio & video specidic fields

  $form['files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Codecs settings'),
    '#weight' => 0,
    '#collapsible' => FALSE,
  );
  // Make the extension list a little more human-friendly by comma-separation.
  $extensions = str_replace(' ', ', ', $settings['file_extensions']);
  $form['files']['file_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed file extensions'),
    '#default_value' => $extensions,
    '#description' => t('Separate extensions with a space or comma and do not include the leading dot.'),
    '#element_validate' => array('_file_generic_settings_extensions'),
    '#weight' => 1,
    // By making this field required, we prevent a potential security issue
    // that would allow files of any type to be uploaded.
    '#required' => TRUE,
  );
  $form['files']['file_directory'] = array(
    '#type' => 'textfield',
    '#title' => t('File directory'),
    '#default_value' => $settings['file_directory'],
    '#description' => t('Optional subdirectory within the upload destination where files will be stored. Do not include preceding or trailing slashes.'),
    '#element_validate' => array('_file_generic_settings_file_directory_validate'),
    '#weight' => 3,
  );
  $form['files']['max_filesize'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum upload size'),
    '#default_value' => $settings['max_filesize'],
    '#description' => t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the allowed file size. If left empty the file sizes will be limited only by PHP\'s maximum post and file upload sizes (current limit <strong>%limit</strong>).', array('%limit' => format_size(file_upload_max_size()))),
    '#size' => 10,
    '#element_validate' => array('_file_generic_settings_max_filesize'),
    '#weight' => 5,
  );

  // Codecs
  $form['codecs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Codecs'),
    '#weight' => 1,
    '#collapsible' => FALSE,
    '#description' => t('Define codecs types available for this type. You can add new codecs types in !link.', array('!link' => l(t('the administration section'), 'admin/config/media/avmedia/codecs'))),
  );
  $form['codecs']['codecs_enabled'] = array(
    '#type' => 'checkboxes',
    '#options' => avmedia_get_codecs_enabled(),
    '#default_value' => array_keys(avmedia_get_codecs_enabled()),
  );

  if ( strcmp( $field['type'], 'avmedia_video') == 0 ) {    
    $form['sfiles'] = array(
        '#type' => 'fieldset',
        '#title' => t('Subtitles settings'),
        '#weight' => 2,
        '#collapsible' => FALSE,
    );
    $extensions = str_replace(' ', ', ', $settings['file_subtitles_extensions']);
    $form['sfiles']['file_subtitles_extensions'] = array(
      '#type' => 'textfield',
      '#title' => t('Allowed file extensions'),
      '#default_value' => $extensions,
      '#description' => t('Separate subtitles extensions with a space or comma and do not include the leading dot.'),
      '#element_validate' => array('_file_generic_settings_extensions'),
      '#weight' => 1,
      // By making this field required, we prevent a potential security issue
      // that would allow files of any type to be uploaded.
      '#required' => TRUE,
    );
    $form['sfiles']['file_subtitles_directory'] = array(
      '#type' => 'textfield',
      '#title' => t('File directory'),
      '#default_value' => $settings['file_subtitles_directory'],
      '#description' => t('Optional subtitles subdirectory within the upload destination where files will be stored. Do not include preceding or trailing slashes.'),
      '#element_validate' => array('_file_generic_settings_file_directory_validate'),
      '#weight' => 2,
    );
    $form['sfiles']['max_filesize_subtitles'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum upload size'),
      '#default_value' => $settings['max_filesize_subtitles'],
      '#description' => t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the allowed subtitles file size. If left empty the file sizes will be limited only by PHP\'s maximum post and file upload sizes (current limit <strong>%limit</strong>).', array('%limit' => format_size(file_upload_max_size()))),
      '#size' => 10,
      '#element_validate' => array('_file_generic_settings_max_filesize'),
      '#weight' => 3,
    );
    $form['subtitles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Subtitles'),
      '#weight' => 3,
      '#collapsible' => FALSE,
      '#description' => t('Define subtitles languages available for this type. You can add new languages in !link.', array('!link' => l(('the administration section'), 'admin/config/media/avmedia/subtitles'))),
    );
    $form['subtitles']['subtitles_enabled'] = array(
      '#type' => 'checkboxes',
      '#options' => avmedia_get_languages_enabled(),
      '#default_value' => array_keys(avmedia_get_languages_enabled()),
    );
  }

  $form['progress_indicator'] = array(
    '#type' => 'radios',
    '#title' => t('Progress indicator'),
    '#options' => array(
      'throbber' => t('Throbber'),
      'bar' => t('Bar with progress meter'),
    ),
    '#default_value' => $settings['progress_indicator'],
    '#description' => t('The throbber display does not show the status of uploads but takes up less space. The progress bar is helpful for monitoring progress on large uploads.'),
    '#weight' => 4,
    '#access' => file_progress_implementation(),
  );

  return $form;
}
/**
 * Implements hook_field_widget_info().
 */
function avmedia_field_widget_info() {
  return array(
    'avmedia_video' => array(
      'label' 			=> t('Video'),
      'field types' => array('avmedia_video'),
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
    'avmedia_audio' => array(
      'label' 			=> t('Audio'),
      'field types' => array('avmedia_audio'),
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}
/**
 * Implements hook_field_formatter_info().
 */
function avmedia_field_formatter_info() {
  return array(
		'avmedia_video_mediaelement' => array(
			'label' 			=> t('MediaElement Video'),
			'field types' => array( 'avmedia_video' ),
		),
		'avmedia_audio_mediaelement' => array(
			'label' 			=> t('MediaElement Audio'),
			'field types' => array( 'avmedia_audio' ),
		),
	);
}
/**
 *  Implements hook_field_widget_view().
 */
function avmedia_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $form_state['avmedia_fields'][$element['#field_name']]['name'] = $element['#field_name'];
  $form_state['avmedia_fields'][$element['#field_name']]['type'] = $instance['widget']['type'];
  if (isset($instance['settings']['codecs'])) {
    $form_state['avmedia_fields'][$element['#field_name']]['codecs'] = $instance['settings']['codecs']['codecs_enabled'];
  }
  if ($instance['widget']['type'] == 'avmedia_video' && isset($instance['settings']['subtitles'])) {
    $form_state['avmedia_fields'][$element['#field_name']]['subtitles'] = $instance['settings']['subtitles']['subtitles_enabled'];
  }
}
/**
 * Implements hook_field_formatter_view().
 */
function avmedia_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, &$items, $display) {
  $element = array();
  
  switch ($field['type']) {
    case 'avmedia_video':
    case 'avmedia_audio':
      $fids = array();
      $name = avmedia_field_name($field);
      
      if (isset($entity->op) && $entity->op == t('Preview')) {
        // Generate preview
        if (isset($entity->{AVC . $name}[LANGUAGE_NONE])) {
          foreach ($entity->{AVC . $name}[LANGUAGE_NONE] as $avc) {
            $delta = 0;
            foreach ($avc[AVFC . $name][LANGUAGE_NONE] as $avfc) {
              if ($avfc[AVFF . $name][LANGUAGE_NONE][0]['fid'] > 0) {
                $items[$delta]['codecs'][$avfc[AVFF . $name][LANGUAGE_NONE][0]['fid']]['fid'] = $avfc[AVFF . $name][LANGUAGE_NONE][0]['fid'];
                $items[$delta]['codecs'][$avfc[AVFF . $name][LANGUAGE_NONE][0]['fid']]['label'] = $avfc[AVFT . $name][LANGUAGE_NONE][0]['value'];
                $fids[$avfc[AVFF . $name][LANGUAGE_NONE][0]['fid']] = $avfc[AVFF . $name][LANGUAGE_NONE][0]['fid'];
                $delta++;
              }
            }
            // Load subtitles if video field
            if ($field['type'] == 'avmedia_video') {
              $delta = 0;
              foreach ($avc[AVSC . $name][LANGUAGE_NONE] as $avsc) {
                if ($avsc[AVSF . $name][LANGUAGE_NONE][0]['fid'] > 0) {
                  $items[$delta]['subtitles'][$avsc[AVSF . $name][LANGUAGE_NONE][0]['fid']]['fid'] = $avsc[AVSF . $name][LANGUAGE_NONE][0]['fid'];
                  $items[$delta]['subtitles'][$avsc[AVSF . $name][LANGUAGE_NONE][0]['fid']]['label'] = $avsc[AVST . $name][LANGUAGE_NONE][0]['value'];
                  $fids[$avsc[AVSF . $name][LANGUAGE_NONE][0]['fid']] = $avsc[AVSF . $name][LANGUAGE_NONE][0]['fid'];
                  $delta++;
                }
              }
            }
          }
        }
      } else {
        // Generate view
        
        $cids = array();
        if (isset($entity->{AVC . $name}[LANGUAGE_NONE])) {
          foreach ($entity->{AVC . $name}[LANGUAGE_NONE] as $item) {
            if (isset($item['value']) && $item['value'] != NULL) {
              $cids[] = $item['value'];
            } elseif (isset($item['entity']) && $item['entity']->item_id != NULL) {
              $cids[] = $item['entity']->item_id;
            }
          }
        }

        
        if (!empty($cids)) {
          // Get codecs and subtitles ids
          $delta = 0;
          foreach (entity_load('field_collection_item', $cids) as $item) {
            // Save ids of codec files
            if (isset($item->{AVFC . $name}[LANGUAGE_NONE])) {
              foreach ($item->{AVFC . $name}[LANGUAGE_NONE] as $cfield) {
                $items[$delta]['codecs'][] = $cfield['value'];
                $fids[] = $cfield['value'];
              }
            }
            // Save ids of subtitles files
            if ($field['type'] == 'avmedia_video') {
              if (isset($item->{AVSC . $name}[LANGUAGE_NONE])) {
                foreach ($item->{AVSC . $name}[LANGUAGE_NONE] as $sfield) {
                  $items[$delta]['subtitles'][] = $sfield['value'];
                  $fids[] = $sfield['value'];
                }
              }
            }
            $delta++;
          }
          
          if (!empty($fids)) {
            $efiles = entity_load('field_collection_item', $fids);     
            dpm($efiles);       
            foreach ($items as $delta => $item) {
              foreach ($item['codecs'] as $id => $cid) {
                unset($items[$delta]['codecs'][$id]);
                if (isset($efiles[$cid]->{AVFF . $name}[LANGUAGE_NONE])) {
                  $items[$delta]['codecs'][$id]['fid'] = $efiles[$cid]->{AVFF . $name}[LANGUAGE_NONE][0]['fid'];
                  $items[$delta]['codecs'][$id]['label'] = $efiles[$cid]->{AVFT . $name}[LANGUAGE_NONE][0]['value'];
                  $files[$efiles[$cid]->{AVFF . $name}[LANGUAGE_NONE][0]['fid']] = (object) $efiles[$cid]->{AVFF . $name}[LANGUAGE_NONE][0];
                }
              }
              if ($field['type'] == 'avmedia_video') {
                foreach ($item['subtitles'] as $id => $cid) {dpm($item);
                  unset($items[$delta]['subtitles'][$id]);
                  if (isset($efiles[$cid]->{AVSF . $name}[LANGUAGE_NONE])) {
                    $items[$delta]['subtitles'][$id]['fid'] = $efiles[$cid]->{AVSF . $name}[LANGUAGE_NONE][0]['fid'];
                    $items[$delta]['subtitles'][$id]['label'] = $efiles[$cid]->{AVST . $name}[LANGUAGE_NONE][0]['value'];
                    $files[$efiles[$cid]->{AVSF . $name}[LANGUAGE_NONE][0]['fid']] = (object) $efiles[$cid]->{AVSF . $name}[LANGUAGE_NONE][0];
                  }
                }
              }
            }
          }
        }
        //dpm(entity_load('field_collection_item', $fids));
        /*$avids = array();
        foreach ($entity->{AVC . $name}[LANGUAGE_NONE] as $av) {
          $avids[$av['value']] = $av['value'];
        }
        
        if (!empty($avids)) {
          $avcids = array();
          foreach (entity_load('field_collection_item', $avids) as $avc) {
            foreach ($avc->{AVFC . $name}[LANGUAGE_NONE] as $item) {
              $avcids[] = $item['value'];
            }
            
            if ($field['type'] == 'avmedia_video') {
              foreach ($avc->{AVSC . $name}[LANGUAGE_NONE] as $item) {
                $avcids[] = $item['value'];
              }
            }
          }
          if (!empty($avcids)) {
            
          }
        }*/
      }
      
      // Load files if there are any fids
      if (!empty($files) || !empty($fids)) {
        if (empty($files)) {
          $files = file_load_multiple($fids);
        }
        $codec_types = avmedia_get_codecs();
        $subtitles_types = avmedia_get_languages();
        // Get audio / video and subtitles files
        foreach ($items as $delta => $item) {
          // Get audio / video files
          if (isset($item['codecs'])) {
            foreach ($item['codecs'] as $file) {
              if (!empty($files[$file['fid']])) {
                $element[$delta]['#files']['codecs'][] = array(
                  'label' => $codec_types[$file['label']]->label,
                  'src' => file_create_url($files[$file['fid']]->uri)
                );
              }
            }
          }
          // Get subtitles files
          if ($field['type'] == 'avmedia_video' && isset($item['subtitles'])) {
            foreach ($item['subtitles'] as $file) {
              if (!empty($files[$file['fid']])) {
                $element[$delta]['#files']['subtitles'][] = array(
                  'label' => $subtitles_types[$file['label']]->name,
                  'src' => file_create_url($files[$file['fid']]->uri)
                );
              }
            }
          }
        
          // Setup theme function
          if (isset($element[$delta]['#files'])) {
            $element[$delta]['#theme'] = $display['type'];
          }
        }
      }
      
      // Sprawdz czy są id bezpośrednio do pobrania
      // Jeśli tak pobierz je i przejsc dzi ids
      // Jeśli nie, pobierz cids, nastepnie z nich pobierz ids
      
      
      
      // Get all entity collections
      /*$cids = array();
      if (isset($entity->{AVC . $name}[LANGUAGE_NONE])) {
        foreach ($entity->{AVC . $name}[LANGUAGE_NONE] as $item) {
          if (isset($item['value']) && $item['value'] != NULL) {
            $cids[] = $item['value'];
          } elseif (isset($item['entity']) && $item['entity']->item_id != NULL) {
            $cids[] = $item['entity']->item_id;
          }
        }
      }
      
      if (count($cids) > 0) {
        // Get codecs and subtitles ids
        $delta = 0;
        $ids = array();
        foreach (entity_load('field_collection_item', $cids) as $item) {
          // Save ids of codec files
          if (isset($item->{AVFC . $name}[LANGUAGE_NONE])) {
            foreach ($item->{AVFC . $name}[LANGUAGE_NONE] as $cfield) {
              $items[$delta]['codecs'][] = $cfield['value'];
              $ids[] = $cfield['value'];
            }
          }
          // Save ids of subtitles files
          if ($field['type'] == 'avmedia_video') {
            if (isset($item->{AVSC . $name}[LANGUAGE_NONE])) {
              foreach ($item->{AVSC . $name}[LANGUAGE_NONE] as $sfield) {
                $items[$delta]['subtitles'][] = $sfield['value'];
                $ids[] = $sfield['value'];
              }
            }
          }
          $delta++;
        }
        
        // Get all entities
        $files = entity_load('field_collection_item', $ids);
        $codec_types = avmedia_get_codecs();
        $subtitles_types = avmedia_get_languages();
        // Get audio / video and subtitles files
        foreach ($items as $delta => $item) {
          // Get audio / video files
          foreach ($item['codecs'] as $id) {
            if (!empty($files[$id]->{AVFF . $name}[LANGUAGE_NONE][0]['uri'])) {
              $element[$delta]['#files']['codecs'][] = array(
                'label' => $codec_types[$files[$id]->{AVFT . $name}[LANGUAGE_NONE][0]['value']]->label,
                'src' => file_create_url($files[$id]->{AVFF . $name}[LANGUAGE_NONE][0]['uri'])
              );
            }
          }
          // Get subtitles files
          if ($field['type'] == 'avmedia_video') {
            foreach ($item['subtitles'] as $id) {
              if (!empty($files[$id]->{AVSF . $name}[LANGUAGE_NONE][0]['uri'])) {
                $element[$delta]['#files']['subtitles'][] = array(
                  'label' => $subtitles_types[$files[$id]->{AVST . $name}[LANGUAGE_NONE][0]['value']]->name,
                  'src' => file_create_url($files[$id]->{AVSF . $name}[LANGUAGE_NONE][0]['uri'])
                );
              }
            }
          }
         
          // Setup theme function
          if (isset($element[$delta]['#files'])) {
            $element[$delta]['#theme'] = $display['type'];
          }
        }
      }*/
      break;
  }
  return $element;
}
/**
 * Implements hook_field_is_empty().
 */
function avmedia_field_is_empty($item, $field) {
  return false;
}

function avmedia_field_widget_form_alter(&$element, &$form_state, $context) {
  if (isset($form_state['avmedia_fields'])) {
    // Get codecs and languages
    $codecs = avmedia_get_codecs();
    $languages = avmedia_get_languages();
    // Setup codecs and subtitles fields labels
    foreach ($form_state['avmedia_fields'] as $field) {
      $name = avmedia_field_name(array('field_name' => $field['name']));
      if (isset($element['#field_name']) && $element['#field_name'] == AVC . $name) {
        // Setup codecs labels
        for ($i = 0; $i < $element[AVFC . $name][LANGUAGE_NONE]['#cardinality']; $i++) {
          if (isset($element['#entity']->is_new) && $element['#entity']->is_new == TRUE) {
            // Add form
            $codec = array_slice($codecs, $i, 1);
            $codec = $codec[0];
          } else {
            // Edit form
            $codec = $codecs[$element[AVFC . $name][LANGUAGE_NONE][$i][AVFT . $name][LANGUAGE_NONE][0]['value']['#default_value']];
          }
          
          $element[AVFC . $name][LANGUAGE_NONE][$i][AVFT . $name][LANGUAGE_NONE][0]['value']['#default_value'] = $codec->id;
          $element[AVFC . $name][LANGUAGE_NONE][$i][AVFF . $name][LANGUAGE_NONE][0]['#title'] = $codec->label;
        }
        if ($field['type'] == 'avmedia_video') {
          // Setup subtitles labels
          for ($i = 0; $i < $element[AVSC . $name][LANGUAGE_NONE]['#cardinality']; $i++) {
            if (isset($element['#entity']->is_new) && $element['#entity']->is_new == TRUE) {
              // Add form
              $lang = array_slice($languages, $i, 1);
              $lang = $lang[key($lang)];
            } else {
              // Edit form
              $lang = $languages[$element[AVSC . $name][LANGUAGE_NONE][$i][AVST . $name][LANGUAGE_NONE][0]['value']['#default_value']];
            }
            
            $element[AVSC . $name][LANGUAGE_NONE][$i][AVST . $name][LANGUAGE_NONE][0]['value']['#default_value'] = $lang->lang;
            $element[AVSC . $name][LANGUAGE_NONE][$i][AVSF . $name][LANGUAGE_NONE][0]['#title'] = $lang->name;
          }
        }
      }
    }
  }
}