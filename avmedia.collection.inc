<?php
/**
 * @file
 * 
 */

// ----------------------------------------------------------------------------
// Hooks
// ----------------------------------------------------------------------------

/**
 * Implements hook_field_create_field().
 */
function avmedia_field_create_field($avfield) {
  switch ($avfield['type']) {
    case 'avmedia_audio':
    case 'avmedia_video':
      // Create collection fields
      foreach (avmedia_installed_collection_fields($avfield) as $field) {
        field_create_field($field);
      }
      // Create new fields
      foreach (avmedia_installed_codec_fields($avfield) as $field) {
        field_create_field($field);
      }
      if ($avfield['type'] == 'avmedia_video') {
        // Create new subtitles fields
        foreach (avmedia_installed_subtitles_fields($avfield) as $field) {
          field_create_field($field);
        }
      }
      // Required for features module
      avmedia_field_update_field($avfield, $avfield);
      break;
  }
}
/**
 * Implements hook_field_update_field().
 */
function avmedia_field_update_field($field, $prior_field) {
  switch ($field['type']) {
    case 'avmedia_audio':
    case 'avmedia_video':
      $name = substr($field['field_name'], 6);
      $cfield = field_info_field(AVC . $name);
      if ($cfield != NULL) {
        $cfield['cardinality'] = $field['cardinality'];
        $cfield['settings']['uri_scheme'] = $field['settings']['uri_scheme'];
        field_update_field($cfield);
      }
      break;
  }
}
/**
 * Implements hook_field_delete_field().
 */
//function avmedia_field_delete_field($field) {
//  switch ($field['type']) {
//    case 'avmedia_audio':
//      break;
//    case 'avmedia_video':
//      break;
//  }
//}
/**
 * Implements hook_field_create_instance().
 */
function avmedia_field_create_instance($avinstance) {
  if (isset($avinstance['widget']) && isset($avinstance['widget']['type'])) {
    switch ($avinstance['widget']['type']) {
      case 'avmedia_audio':
      case 'avmedia_video':
        // Create new collection fields instances
        foreach (avmedia_installed_collection_fields_instances($avinstance) as $instance) {
          field_create_instance($instance);
        }
        // Create new fields instances
        foreach (avmedia_installed_codec_fields_instances($avinstance) as $instance) {
          field_create_instance($instance);
        }
        if ($avinstance['widget']['type'] == 'avmedia_video') {
          // Create new subtitles fields instances
          foreach (avmedia_installed_subtitles_fields_instances($avinstance) as $instance) {
            field_create_instance($instance);
          }
        }
        // Required for features module
        avmedia_field_update_instance($avinstance, $avinstance);
        break;
    }
  }
}
/**
 * Implements hook_field_update_instance().
 */
function avmedia_field_update_instance($instance, $prior_instance) {
  switch ($instance['widget']['type']) {
    case 'avmedia_audio':
    case 'avmedia_video':
      $name = substr($instance['field_name'], 6);
      // Files instance
      // Update cardinality based on enabled codecs for instance
      $fcfield = field_info_field(AVFC . $name);
      if ($fcfield !== NULL) {
		if (isset($instance['settings'])) {
		  $cardinality = 0;
          foreach ($instance['settings']['codecs']['codecs_enabled'] as $codec) {
            if ($codec > 0) {
              $cardinality++;
            }
          }
          $fcfield['cardinality'] = $cardinality;
          field_update_field($fcfield);
		}
      }
      
      // Update files settings
      $finstance = field_info_instance('field_collection_item', AVFF . $name, AVFC . $name);
      if ($finstance !== NULL && isset($instance['settings'])) {
        $finstance['settings']['file_extensions'] = $instance['settings']['files']['file_extensions'];
        $finstance['settings']['file_directory'] = $instance['settings']['files']['file_directory'];
        $finstance['settings']['max_filesize'] = $instance['settings']['files']['max_filesize'];
        $finstance['settings']['progress_indicator'] = $instance['settings']['progress_indicator'];
        if (isset($instance['widget']['settings'])) {
          $finstance['widget']['settings']['progress_indicator'] = $instance['settings']['progress_indicator'];
        }
        field_update_instance($finstance);
      }
              
      if ($instance['widget']['type'] == 'avmedia_video') {
        // Subtitles instances
        
        // Update cardinality based on enabled codecs for instance
        $scfield = field_info_field(AVSC . $name);
        if ($scfield !== NULL) {
		  if (isset($instance['settings'])) {
            $cardinality = 0;
            foreach ($instance['settings']['subtitles']['subtitles_enabled'] as $subtitles) {
              if (!is_numeric($subtitles)) {
                $cardinality++;
              }
            }
            $scfield['cardinality'] = $cardinality;
            field_update_field($scfield);
          }
		}
        
        // Update files settings
        $sinstance = field_info_instance('field_collection_item', AVSF . $name, AVSC . $name);
        if ($sinstance !== NULL && isset($instance['settings'])) {
          $sinstance['settings']['file_extensions'] = $instance['settings']['sfiles']['file_subtitles_extensions'];
          $sinstance['settings']['file_directory'] = $instance['settings']['sfiles']['file_subtitles_directory'];
          $sinstance['settings']['max_filesize'] = $instance['settings']['sfiles']['max_filesize_subtitles'];
          $sinstance['settings']['progress_indicator'] = $instance['settings']['progress_indicator'];
          if (isset($instance['widget']['settings'])) {
            $sinstance['widget']['settings']['progress_indicator'] = $instance['settings']['progress_indicator'];
          }
          field_update_instance($sinstance);
        }
      }
      break;
  }
}
/**
 * Implements hook_field_delete_instance().
 */
//function avmedia_field_delete_instance($instance) {
//  if (isset($instance['type'])) {
//    switch ($instance['type']) {
//      case 'avmedia_audio':
//        break;
//      case 'avmedia_video':
//        break;
//    }
//  }
//}

// ----------------------------------------------------------------------------
// Define field collection items
// ----------------------------------------------------------------------------

/**
 *
 */
function avmedia_installed_collection_fields($field = NULL) {
  $name = '';
  if ($field !== NULL) {
    $name = avmedia_field_name($field);
  }

  return array(
    AVC . $name => array(
      'field_name'  => AVC . $name,
      'cardinality' => 1,
      'type'        => 'field_collection',
      'settings'    => array('hide_blank_items' => 0),
    ),
  );
}
/**
 *
 */
function avmedia_installed_collection_fields_instances($instance = NULL) {
  $t = get_t();
  $name = '';
  $type = '';
  if ($instance !== NULL) {
    $name = avmedia_field_name($instance);
    $type = ($instance['widget']['type'] == 'avmedia_video') ? $t('Video') : $t('Audio');
  }

  return array(
    AVC . $name => array(
      'entity_type' => $instance['entity_type'],
      'bundle' => $instance['bundle'],
      'field_name'  => AVC . $name,
      'label'       => $t('@type files', array('@type' => $type)),
      'widget'      => array(
        'type'    => 'field_collection_embed',
        'settings'    => array(),
        'weight' => -10,
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
          'settings' => array(),
          'weight' => -10,
        ),
        'teaser' => array(
          'type' => 'hidden',
          'settings' => array(),
          'weight' => -10,
        ),
      ),
      'required' => FALSE,
    ),
  );
}
/**
 *
 */
function avmedia_installed_codec_fields($field = NULL) {
  $name = '';
  if ($field !== NULL) {
    $name = avmedia_field_name($field);
  }

  return array(
    AVFC . $name => array(
      'field_name'  => AVFC . $name,
      'cardinality' => count(avmedia_get_codecs_enabled()),
      'type'        => 'field_collection',
      'settings'    => array('hide_blank_items' => 0),
    ),
    AVFT . $name => array(
      'field_name'  => AVFT . $name,
      'cardinality' => 1,
      'type'        => 'field_hidden_integer',
      'settings'    => array(),
      'active'      => 1,
    ),
    AVFF  . $name => array(
      'field_name'  => AVFF . $name,
      'cardinality' => 1,
      'type'        => 'file',
      'settings'    => array(
        'display_field' => 0,
        'display_default' => 0,
        'uri_scheme' => 'public',
      ),
      'active'      => 1,
    ),
  );
}
/**
 *
 */
function avmedia_installed_codec_fields_instances($instance = NULL) {
  $t = get_t();
  $name = '';
  if ($instance !== NULL) {
    $name = avmedia_field_name($instance);
  }
  
  return array(
    AVFC . $name => array(
      'entity_type' => 'field_collection_item',
      'bundle' => AVC . $name,
      'field_name'  => AVFC . $name,
      'label'       => $t('Files'),
      'widget'      => array(
        'type'    => 'field_collection_embed',
        'settings'    => array(),
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
        'teaser' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
      ),
      'required' => FALSE,
    ),
    AVFT . $name => array(
      'entity_type' => 'field_collection_item',
      'bundle' => AVFC . $name,
      'field_name'  => AVFT . $name,
      'widget'      => array(
        'type'        => 'field_hidden',
        'active'      => 0,
        'settings'    => array(),
        'weight'      => 0,
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
        'teaser' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
      ),
      'required' => FALSE,
    ),
    AVFF . $name => array(
      'entity_type' => 'field_collection_item',
      'bundle' => AVFC . $name,
      'field_name'  => AVFF . $name,
      'widget'      => array(
        'type'        => 'file_generic',
        'active'      => 1,
        'settings'    => array('progress_indicator' => 'throbber', 'imce_filefield_on' => 1),
        'weight'      => 1,
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
        'teaser' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
      ),
      'required' => FALSE,
    ),
  );
}
/**
 *
 */
function avmedia_installed_subtitles_fields($field) {
  $name = '';
  if ($field !== NULL) {
    $name = avmedia_field_name($field);
  }
  
  return array(
    AVSC . $name => array(
      'field_name'  => AVSC . $name,
      'cardinality' => count(avmedia_get_languages_enabled()),
      'type'        => 'field_collection',
      'settings'    => array('hide_blank_items' => 0),
    ),
    AVST . $name => array(
      'field_name'  => AVST . $name,
      'cardinality' => 1,
      'type'        => 'field_hidden_text',
      'settings'    => array(),
      'active'      => 1,
    ),
    AVSF  . $name => array(
      'field_name'  => AVSF . $name,
      'cardinality' => 1,
      'type'        => 'file',
      'settings'    => array(
        'display_field' => 0,
        'display_default' => 0,
        'uri_scheme' => 'public',
      ),
      'active'      => 1,
    ),
  );
}
/**
 *
 */
function avmedia_installed_subtitles_fields_instances($instance = NULL) {
  $t = get_t();
  $name = '';
  if ($instance !== NULL) {
    $name = avmedia_field_name($instance);
  }
  
  return array(
    AVSC . $name => array(
      'entity_type' => 'field_collection_item',
      'bundle' => AVC . $name,
      'field_name'  => AVSC . $name,
      'label'       => $t('Subtitles'),
      'widget'      => array(
        'type'    => 'field_collection_embed',
        'settings'    => array(),
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
        'teaser' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
      ),
      'required' => FALSE,
    ),
    AVST . $name => array(
      'entity_type' => 'field_collection_item',
      'bundle' => AVSC . $name,
      'field_name'  => AVST . $name,
      'widget'      => array(
        'type'        => 'field_hidden',
        'active'      => 0,
        'settings'    => array(),
        'weight'      => 0,
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
        'teaser' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
      ),
      'required' => FALSE,
    ),
    AVSF . $name => array(
      'entity_type' => 'field_collection_item',
      'bundle' => AVSC . $name,
      'field_name'  => AVSF . $name,
      'widget'      => array(
        'type'        => 'file_generic',
        'active'      => 1,
        'settings'    => array('progress_indicator' => 'throbber', 'imce_filefield_on' => 1),
        'weight'      => 1,
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
        'teaser' => array(
          'type' => 'hidden',
          'settings'    => array(),
        ),
      ),
      'required' => FALSE,
    ),
  );
}

// ----------------------------------------------------------------------------
// Helper functions
// ----------------------------------------------------------------------------

/**
 *
 */
function avmedia_field_name($field) {
  return substr($field['field_name'], 6);
}
